package com.chhaya.mvvmafternoon.helpers;

import android.util.Log;

import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OneSignalHelper {

    public static void postNotificationByPlayerIds(JSONObject jsonObject){

        OneSignal.postNotification(jsonObject, new OneSignal.PostNotificationResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d("tag success", response.toString());
            }

            @Override
            public void onFailure(JSONObject response) {
                Log.e("tag fail", response.toString());
            }
        });
    }

}
