package com.chhaya.mvvmafternoon.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chhaya.mvvmafternoon.models.api.entities.Pagination;
import com.chhaya.mvvmafternoon.models.api.repositories.ArticleRepository;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;

public class ListArticleViewModel extends ViewModel {

    private MutableLiveData<ListArticleResponse> liveData;
    private ArticleRepository articleRepository;
    private Pagination pagination;

    public void init() {
        articleRepository = new ArticleRepository();
        pagination = new Pagination();
        liveData = articleRepository.getListArticleByPagination(pagination);
    }

    public MutableLiveData<ListArticleResponse> getLiveData() {
        return liveData;
    }


}
