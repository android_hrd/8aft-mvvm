package com.chhaya.mvvmafternoon.views.article;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;

import com.chhaya.mvvmafternoon.R;
import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.viewmodels.ListArticleViewModel;

public class ListArticleActivity extends AppCompatActivity {

    private ListArticleResponse dataSet;
    private RecyclerView rcvListArticle;
    private SwipeRefreshLayout swipeContainer;
    private ListArticleAdapter adapter;

    private ListArticleViewModel listArticleViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);

        rcvListArticle = findViewById(R.id.rcv_list_article);

        setupSwipeRefreshLayout();

        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();

        getLiveDataFromViewModel();

    }

    private void setupRecyclerView() {
        adapter = new ListArticleAdapter(this, dataSet.getData());
        rcvListArticle.setLayoutManager(new LinearLayoutManager(this));
        rcvListArticle.setAdapter(adapter);
    }

    private void setupSwipeRefreshLayout() {
        swipeContainer = findViewById(R.id.swipe_container);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLiveDataFromViewModel();
            }
        });
    }

    private void getLiveDataFromViewModel() {
        listArticleViewModel.getLiveData().observe(this, new Observer<ListArticleResponse>() {
            @Override
            public void onChanged(ListArticleResponse listArticleResponse) {
                Log.d("tag change", "onChange");
                dataSet = listArticleResponse;
                setupRecyclerView();
                swipeContainer.setRefreshing(false);
            }
        });
    }

}
