package com.chhaya.mvvmafternoon.views.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.chhaya.mvvmafternoon.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private LoginButton btnFbLogin;

    private final static String EMAIL = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // TODO: implement facebook login:
        callbackManager = CallbackManager.Factory.create();

        btnFbLogin = findViewById(R.id.login_button);
        btnFbLogin.setReadPermissions(Arrays.asList(EMAIL));

        btnFbLogin.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("tag success", "Logged in successfully");
                        Log.d("tag success", loginResult.getAccessToken().getUserId());
                    }

                    @Override
                    public void onCancel() {
                        Log.d("tag cancel", "Logged in is cancelled");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("tag error", "Logged in is error");
                        Log.e("tag error", "Error : " + error.getMessage());
                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
