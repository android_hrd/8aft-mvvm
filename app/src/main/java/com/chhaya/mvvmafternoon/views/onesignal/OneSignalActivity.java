package com.chhaya.mvvmafternoon.views.onesignal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chhaya.mvvmafternoon.R;
import com.chhaya.mvvmafternoon.helpers.OneSignalHelper;
import com.chhaya.mvvmafternoon.models.entities.OneSignalNotification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OneSignalActivity extends AppCompatActivity {

    private EditText editMessage;
    private Button btnSendMessage;

    private String[] playerIds = {"6eb039ff-0e9e-4644-9738-f7d70a126355"};

    private void initViews() {
        editMessage = findViewById(R.id.edit_message);
        btnSendMessage = findViewById(R.id.button_send_message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_signal);

        initViews();

        btnSendMessage.setOnClickListener(v -> {
            // TODO : push notification to player by playerId

            OneSignalNotification obj = new OneSignalNotification();

            try {

                obj.setAppId("54661f97-cb63-460a-b321-2eecd9c9b834");
                obj.setPlayerIds(
                        new JSONArray().put(playerIds[0]));
                obj.setContents(new JSONObject().put("en",
                        editMessage.getText().toString()));
                obj.setHeadings(new JSONObject().put("en",
                        "Congratulation!"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OneSignalHelper.postNotificationByPlayerIds(obj.toJsonObject());
        });
    }
}
