package com.chhaya.mvvmafternoon.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.chhaya.mvvmafternoon.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
