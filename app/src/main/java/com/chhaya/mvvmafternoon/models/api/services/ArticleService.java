package com.chhaya.mvvmafternoon.models.api.services;

import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ListArticleResponse> getAll(@Query("page") long page,
                                     @Query("limit") long limit);

}
