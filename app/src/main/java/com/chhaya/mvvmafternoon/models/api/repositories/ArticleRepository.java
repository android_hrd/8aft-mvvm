package com.chhaya.mvvmafternoon.models.api.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.chhaya.mvvmafternoon.models.api.config.ServiceGenerator;
import com.chhaya.mvvmafternoon.models.api.entities.Pagination;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.models.api.services.ArticleService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public MutableLiveData<ListArticleResponse> getListArticleByPagination(Pagination pagination) {

        final MutableLiveData<ListArticleResponse> liveData = new MutableLiveData<>();

        // TODO implement retrofit
        Call<ListArticleResponse> call = articleService.getAll(pagination.getPage(), pagination.getLimit());

        call.enqueue(new Callback<ListArticleResponse>() {
            @Override
            public void onResponse(Call<ListArticleResponse> call, Response<ListArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListArticleResponse> call, Throwable t) {
                Log.e("tag error", t.getMessage());
            }
        });


        return liveData;

    }


}
